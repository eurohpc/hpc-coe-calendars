from bs4 import BeautifulSoup
from html_sanitizer import Sanitizer
from htmlmin import minify
import hashlib
from ics import Calendar
# from ics.parse import ContentLine
import requests
import os
import json
import re
from PIL import Image
import requests
from io import BytesIO
import random
import string
import datetime

url = "https://timelyapp.time.ly/api/calendars/22028977/export?format=ics"
calendar = Calendar(requests.get(url).text)
sanitizer = Sanitizer()
event_container = {"events": []}
unordered_events = []


def get_attribute(event, attribute):
    result = ""
    if hasattr(event, attribute):
        result = str(getattr(event, attribute))
    return result


def ical_event_to_json(event):
    result = {}
    result["name"] = get_attribute(event, "summary")
    result["startDate"] = get_attribute(event, "begin")
    result["endDate"] = get_attribute(event, "end")
    # If end date is already passed just return already
    tz_date = datetime.datetime.fromisoformat(result["endDate"])
    tz_now = datetime.datetime.now(tz_date.tzinfo)
    if tz_date < tz_now:
        return None

    result["description"] = get_attribute(event, "description")
    result["location"] = get_attribute(event, "location")
    result["url"] = get_attribute(event, "url")
    result["teaches"] = get_attribute(event, "categories")
    result["eventStatus"] = get_attribute(event, "status")
    result["organizer"] = get_attribute(event, "organizer")
    result["educationalLevel"] = get_attribute(event, "classification")

    # Find the original event page (custom for calendar above)
    result["hostUrl"] = re.findall(r"X-TICKETS-URL:.*", str(event.extra))
    if not result["hostUrl"]:
        result["hostUrl"] = result["url"]
    else:
        result["hostUrl"] = result["hostUrl"][0].replace("X-TICKETS-URL:", "").rstrip()
    # Hash the event URL so we have a unique identifier
    result["hash"] = hashlib.md5(result["hostUrl"].encode()).hexdigest()

    # Find an image for the event (custom for calendar above)
    result["image"] = re.findall(r"X-WP-IMAGES-URL:.*", str(event.extra))
    if result["image"]:
        # Let's cache a compressed version of the image
        try:
            response = requests.get(
                result["image"][0].replace("X-WP-IMAGES-URL:", "").rstrip()
            )
        except Exception as error:
            # Ignore any exceptions
            print("Exception as warning: ", error)
            response = None
        # If we get a non-error response, let's cache the image
        if response:
            image = Image.open(BytesIO(response.content))
            image = image.convert("RGBA")
            image.thumbnail((384, 384), Image.ANTIALIAS)
            final_image_path = os.path.join(
                "tmp_images",
                "".join(result["hash"])
                + ".webp",
            )
            image.save(final_image_path, "webp")
            result["image"] = final_image_path
        else:
            result["image"] = None

    if not result["image"]:
        if "events.prace-ri" in result["hostUrl"]:
            result["image"] = "images/PRACE_placeholder-sm.webp"
        else:
            # reuse the banner image
            result["image"] = "images/hero-sm.webp"

    return result


for event in calendar.events:
    # Grab the html event page
    # soup = BeautifulSoup(requests.get(event.url).text, 'html.parser')
    # Search for the div that contains the description and use the html there
    # as the description for our event
    # event.description = minify(sanitizer.sanitize(str(soup.find(itemprop="description"))))

    # sanitize the name (just in case)
    event.summary = minify(sanitizer.sanitize(event.summary))
    # Prepare the event description for dumping (only keeping the first 150 characters)
    event.description = json.dumps(minify(sanitizer.sanitize(event.description))[0:150])[1:-1]

    json_event = ical_event_to_json(event)
    if json_event:
        unordered_events.append(json_event)


with open(os.path.join("events.ics"), "w") as f:
    f.write(str(calendar))
with open(os.path.join("events.json"), "w") as f:
    # Sort the list events by start date
    event_container["events"] = sorted(unordered_events, key=lambda k: k["startDate"])
    f.write("var eventJS = '")
    f.write(json.dumps(event_container).replace("'", "\\\'"))
    f.write("'")
